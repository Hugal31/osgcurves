#version 330

#define DEBUG_N_ITERATIONS 0
const uint MAX_BISEC_ITERATIONS = 6u;

/// @brief Number of trimming curve, lenght nurbs_trimming_desc
uniform int nurbs_n_trimming;
/// @brief Contains nurbs_n_trimming ivec4, which contains the order of the curve and the index of its first control point.
/// @todo Use other thing than ivec4?
uniform isamplerBuffer nurbs_trimming_desc;
/// @brief Contains the control points of the curve
uniform samplerBuffer nurbs_trimming_data;

uniform int nurbs_n_v_intervals;
/// @brief Contains the definition of the V intervals. [0] and [1] contains the bounds of the interval,
///        [2] and [3] The [begin, end) index U interval in nurbs_u_intervals_desc.
uniform samplerBuffer nurbs_v_intervals_desc;

/// @brief Contains the definition of the U intervals within V intervals. [0] and [1] contains the bounds of the interval.
uniform samplerBuffer nurbs_u_intervals_desc;
/// @brief Contains the data of the U intervals. [0] and [1] of each elements contains the [begin, end) index in the curve list of the curves to consider.
///        [2] The number of intersections already classified.
uniform isamplerBuffer nurbs_u_intervals_data;

/// @brief Same format as nurbs_trimming_data, but should not be iterated entirely.
uniform isamplerBuffer nurbs_curve_list;

in vec2 f_uvCoord;

out vec4 fragColor;

// TODO Use include?
#ifndef GLSL_SWAP
#define GLSL_SWAP

#define DEFINE_SWAP(type) void swap(inout type a, inout type b) \
{ \
    type tmp = a; \
    a = b; \
    b = tmp; \
}

DEFINE_SWAP(float)
DEFINE_SWAP(vec2)

#undef DEFINE_SWAP

#endif

#ifndef GLSL_HORNER_CURVE
#define GLSL_HORNER_CURVE

/**
 * @brief Evaluate a 2D bezier curve using the horner algorithm
 * @param data Buffer of vec4, which are vec3 with an extra member set to 0,
 *             holding 2D coodinate in the homogenous space ([wx, wy, w])
 * @param index Index of the first control point of the curve in data.
 * @param order Order of the curve, i.e. the number of control points
 * @param t     Coordinate to evaluate the curve
 */
vec2 hornerBezierEvaluation(in samplerBuffer data, in int index, in int order, in float t)
{
    int deg = order - 1;
    vec4 p;

    if (order == 1) {
        // Linear interpolation
        p = mix(texelFetch(data, index), texelFetch(data, index + 1), t);
    } else {
        float u = 1.0 - t;

        float bc = 1.0;
        float tn = 1.0;

        p = u * texelFetch(data, index);

        for (int i = 1; i < deg; ++i) {
            tn *= t;
            bc *= float(deg - i + 1) / float(i);
            p = u * (p + tn * bc * texelFetch(data, index + i));
        }

        p += tn * t * texelFetch(data, index + deg);
    }

    // Project into euclidian space
    return vec2(p[0] / p[2], p[1] / p[2]);
}

/// @brief Evaluate a bezier curve with special cases 0 and 1
vec2 hornerBezierEvaluationWithSpecial(in samplerBuffer data, in int index, in int order, in float t) {
    if (t == 0.0f) {
        vec4 p = texelFetch(data, index);
        return vec2(p[0] / p[2], p[1] / p[2]);
    } else if (t == 1.0f) {
        vec4 p = texelFetch(data, index + order - 1);
        return vec2(p[0] / p[2], p[1] / p[2]);
    } else {
        return hornerBezierEvaluation(data, index, order, t);
    }
}

#endif

/**
 * @brief Do a bisection over a bi-monotonic curve.
 * @note Do not use the acceleration parameters
 * @todo Document the params
 * @todo Change to a better name
 * @returns True if f_uvCoord is at the left of the curve identified by the index
 */
bool isIntersecting(in samplerBuffer data, in int index, in int order, out uint nIterations) {
    nIterations = 1u;

    float low = 0.0f;
    float high = 1.0f;

    vec2 lowValue = hornerBezierEvaluationWithSpecial(data, index, order, low);
    vec2 highValue = hornerBezierEvaluationWithSpecial(data, index, order, high);

    // If decreasing over y, swap low and high
    if (lowValue.y > highValue.y) {
        swap(low, high);
        swap(lowValue, highValue);
    }

    // If under of over the curve, skip
    if (f_uvCoord.y < lowValue.y || f_uvCoord.y > highValue.y)
        return false;

    for (; nIterations <= MAX_BISEC_ITERATIONS; ++nIterations) {
        // Compute the AABB
        float left = min(lowValue.x, highValue.x);
        float right = max(lowValue.x, highValue.x);

        // Check if we are oustside of the bounding box.
        if (f_uvCoord.x > right) {
            return false;
        } else if (f_uvCoord.x < left) {
            return true;
        }

        // Compute next point
        float mid = (low + high) / 2.0f;
        vec2 midValue = hornerBezierEvaluation(data, index, order, mid);
        if (f_uvCoord.y < midValue.y) {
            high = mid;
            highValue = midValue;
        } else {
            low = mid;
            lowValue = midValue;
        }
    }

    // Do a linear interpolation if we reach the maximum number of iteration
    float a = (highValue.x - lowValue.x) / (highValue.y - lowValue.y);
    float b = lowValue.x -a * lowValue.y;

    return f_uvCoord.x < a * f_uvCoord.y + b;
}

uint testTrimmingAllCurves(out uint nIterations)
{
    nIterations = 0u;
    uint nIntersection = 0u;
    for (int i = 0; i < nurbs_n_trimming; ++i)
    {
        ivec4 bezierDesc = texelFetch(nurbs_trimming_desc, i);

        uint tmp;
        if (isIntersecting(nurbs_trimming_data, bezierDesc.y, bezierDesc.x, tmp))
            ++nIntersection;
        nIterations += tmp;
    }

    return nIntersection;
}

/**
 * @brief Find an interval in an interval list.
 * @param intervals List of intervals. [0] item of the elements must contains lower bound and [1] the upper bound.
 * @param itStart Begining iterator of the interval list
 * @param itEnd End iterator of the interval list (excluded)
 * @param value Value to find in the interval list.
 * @return True if the interval was found.
 */
bool findInterval(in samplerBuffer intervals, in int itStart, in int itEnd, in float value, inout vec4 interval, out int intervalIndex)
{
    int indexMin = itStart;
    int indexMax = itEnd - 1;

    while (indexMin <= indexMax) {
        intervalIndex = indexMin + (indexMax - indexMin) / 2;

        interval = texelFetch(intervals, intervalIndex);

        if (value >= interval[0] && value <= interval[1])
            return true;

        if (value < interval[0]) {
            indexMax = intervalIndex - 1;
        } else {
            indexMin = intervalIndex + 1;
        }
    }

    return false;
}

uint testTrimmingBisection(out uint nIterations)
{
    nIterations = 0u;

    vec4 vInterval;
    int vIntervalIndex;
    if (!findInterval(nurbs_v_intervals_desc, 0, nurbs_n_v_intervals, f_uvCoord.y, vInterval, vIntervalIndex)) {
        return 0u;
    }

    int uIntervalIt = floatBitsToInt(vInterval[2]);
    int uIntervalEnd = floatBitsToInt(vInterval[3]);

    vec4 uInterval;
    int uIntervalIndex;
    if (!findInterval(nurbs_u_intervals_desc, uIntervalIt, uIntervalEnd, f_uvCoord.x, uInterval, uIntervalIndex)) {
        return 0u;
    }

    ivec4 uIntervalData = texelFetch(nurbs_u_intervals_data, uIntervalIndex);

    int curvesIt = uIntervalData[0];
    int curvesEnd = uIntervalData[1];

    uint nIntersections = uint(uIntervalData[2]);

    for (; curvesIt != curvesEnd; ++curvesIt) {
        ivec4 bezierDesc = texelFetch(nurbs_curve_list, curvesIt);

        uint tmp;
        if (isIntersecting(nurbs_trimming_data, bezierDesc.y, bezierDesc.x, tmp))
            ++nIntersections;
        nIterations += tmp;
    }

    return nIntersections;
}

bool canDisplay()
{
    uint nIterations;
    return testTrimmingBisection(nIterations) % 2u != 0u;
}

void main()
{
#if DEBUG_N_ITERATIONS
    uint nIterations;
    bool display = testTrimmingBisection(nIterations) % 2 != 0;

    if (nIterations == 0) {
        if (display)
            fragColor = vec4(f_uvCoord.x, 0.0f, f_uvCoord.y, 1.0f);
        else
            discard;
    } else {
        float green = display ? 0.8 : 0.0;
        float red = !display ? 0.8 : 0.0;
        fragColor = mix(vec4(red, green, 0.0, 1.0), vec4(0.0, 0.0, 0.0, 1.0), float(nIterations) / float(MAX_BISEC_ITERATIONS));
    }
#else
    if (!canDisplay())
        discard;

    fragColor = vec4(f_uvCoord.x, 0.0f, f_uvCoord.y, 1.0f);
#endif
}
