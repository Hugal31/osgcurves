#version 330

in vec2 uvCoord;

out vec2 f_uvCoord;

void main(void)
{
    f_uvCoord = uvCoord;
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
