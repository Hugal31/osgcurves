#ifndef GLSL_HORNER_CURVE
#define GLSL_HORNER_CURVE

/**
 * @brief Evaluate a 2D bezier curve using the horner algorithm
 * @param data Buffer of vec4, which are vec3 with an extra member set to 0,
 *             holding 2D coodinate in the homogenous space ([wx, wy, w])
 * @param index Index of the first control point of the curve in data.
 * @param order Order of the curve, i.e. the number of control points
 * @param t     Coordinate to evaluate the curve
 */
vec2 hornerBezierEvaluation(in samplerBuffer data, in int index, in int order, in float t)
{
    int deg = order - 1;
    vec4 p;

    if (order == 1)  {
        // Linear interpolation
        p = mix(texelFetch(data, index), texelFetch(data, index + 1), t);
    } else {
        float u = 1.0 - t;

        float bc = 1.0;
        float tn = 1.0;

        p = u * texelFetch(data, index);

        for (int i = 1; i <= deg - 1; ++i) {
            tn *= t;
            bc *= float(deg - i + 1) / float(i);
            p = u * (p + tn * bc * texelFetch(data, index + i));
        }

        p += tn * t * texelFetch(data, index + deg);
    }

    // Project into euclidian space
    return vec2(p[0] / p[2], p[1] / p[2]);
}

#endif
