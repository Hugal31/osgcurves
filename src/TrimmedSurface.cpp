#include <algorithm>
#include <iostream>

#include <osg/BufferTemplate>
#include <osg/BufferIndexBinding>
#include <osg/TextureBuffer>
#include <osgDB/ReadFile>

#include "osgNurbs/partition/serialize/BinaryPartitionSerializer.h"
#include "osgNurbs/TrimmedSurface.h"

namespace osg
{
namespace nurbs
{

////////////////////////////////////////////////////////////////////////////////
TrimmedSurface::TrimmedSurface(std::vector<CurveDef> trimmingCurves, LC::BinaryPartition<float> partition)
        : _trimmingCurves { std::move(trimmingCurves) }
        , _partition { std::move(partition) }
{
    initializeSurface();
}

////////////////////////////////////////////////////////////////////////////////
TrimmedSurface::TrimmedSurface(const TrimmedSurface &other, const CopyOp &copyop)
        : Geometry { other, copyop }
        , _trimmingCurves { other._trimmingCurves }
        , _partition { other._partition }
{

}

////////////////////////////////////////////////////////////////////////////////
void TrimmedSurface::initializeSurface()
{
    ref_ptr<Shader> vertexShader = osgDB::readShaderFile("trimmed_surface/trimmed_surface.vert.glsl");
    assert(vertexShader.valid());

    vertexShader->setType(Shader::Type::VERTEX);
    ref_ptr<Shader> fragmentShader = osgDB::readShaderFile("trimmed_surface/trimmed_surface.frag.glsl");
    assert(fragmentShader.valid());
    fragmentShader->setType(Shader::Type::FRAGMENT);

    ref_ptr<Program> program = new Program;
    program->addShader(fragmentShader);
    program->addShader(vertexShader);

    StateSet *stateSet = getOrCreateStateSet();
    stateSet->setAttributeAndModes(program.get());

    auto *surfaceVertices = new Vec3Array;
    surfaceVertices->push_back(Vec3(0, 0, 0));
    surfaceVertices->push_back(Vec3(1, 0, 0));
    surfaceVertices->push_back(Vec3(0, 1, 0));
    surfaceVertices->push_back(Vec3(1, 1, 0));

    {
        auto *uvCoords = new Vec2Array;
        uvCoords->push_back(Vec2(0, 0));
        uvCoords->push_back(Vec2(1, 0));
        uvCoords->push_back(Vec2(0, 1));
        uvCoords->push_back(Vec2(1, 1));
        program->addBindAttribLocation("uvCoord", 13);
        setVertexAttribArray(13, uvCoords, Array::Binding::BIND_PER_VERTEX);
    }

    {
        auto *da = new DrawArrays(osg::PrimitiveSet::TRIANGLE_STRIP, 0, static_cast<GLsizei>(surfaceVertices->size()));
        setVertexArray(surfaceVertices);
        addPrimitiveSet(da);
        setUseDisplayList(false);
    }

    LC::BinaryPartitionSerializer::SerializedPartition serializedPartition = LC::BinaryPartitionSerializer::serialize(_partition);

    stateSet->addUniform(new Uniform("nurbs_n_trimming", static_cast<int>(serializedPartition.trimmingCurvesDesc.size())));

    // Desc
    auto *trimmingDescBufferData = new BufferTemplate<std::vector<osg::Vec4i> >;
    trimmingDescBufferData->setData(serializedPartition.trimmingCurvesDesc);
    auto *trimmingDescBuffer = new TextureBuffer(trimmingDescBufferData);
    trimmingDescBuffer->setInternalFormat(GL_RGBA32I);
    stateSet->setTextureAttribute(1, trimmingDescBuffer);

    auto *trimmingDescUniform = new Uniform(Uniform::Type::INT_SAMPLER_BUFFER, "nurbs_trimming_desc", 1);
    trimmingDescUniform->set(1);
    stateSet->addUniform(trimmingDescUniform);

    // Data
    auto *trimmingDataBufferData = new BufferTemplate<std::vector<osg::Vec4f> >;
    trimmingDataBufferData->setData(serializedPartition.trimmingCurvesData);
    auto *trimmingDataBuffer = new TextureBuffer(trimmingDataBufferData);
    trimmingDataBuffer->setInternalFormat(GL_RGBA32F);
    stateSet->setTextureAttribute(2, trimmingDataBuffer);

    auto *trimmingDataUniform = new Uniform(Uniform::Type::INT_SAMPLER_BUFFER, "nurbs_trimming_data", 1);
    trimmingDataUniform->set(2);
    stateSet->addUniform(trimmingDataUniform);

    stateSet->addUniform(new Uniform("nurbs_n_v_intervals", static_cast<int>(serializedPartition.vIntervalsDesc.size())));

    // Add nurbs_v_intervals_desc
    auto vIntervalDescBufferData = new BufferTemplate<decltype(serializedPartition.vIntervalsDesc)>;
    vIntervalDescBufferData->setData(serializedPartition.vIntervalsDesc);
    auto vIntervalDescBuffer = new TextureBuffer(vIntervalDescBufferData);
    vIntervalDescBuffer->setInternalFormat(GL_RGBA32F);
    stateSet->setTextureAttribute(3, vIntervalDescBuffer);

    auto vIntervalDataUniform = new Uniform(Uniform::Type::SAMPLER_BUFFER, "nurbs_v_intervals_desc", 1);
    vIntervalDataUniform->set(3);
    stateSet->addUniform(vIntervalDataUniform);

    // Add nurbs_u_intervals_desc
    auto uIntervalDescBufferData = new BufferTemplate<decltype(serializedPartition.uIntervalsDesc)>;
    uIntervalDescBufferData->setData(serializedPartition.uIntervalsDesc);
    auto uIntervalDescBuffer = new TextureBuffer(uIntervalDescBufferData);
    uIntervalDescBuffer->setInternalFormat(GL_RGBA32F);
    stateSet->setTextureAttribute(4, uIntervalDescBuffer);

    auto uIntervalDescUniform = new Uniform(Uniform::Type::SAMPLER_BUFFER, "nurbs_u_intervals_desc", 1);
    uIntervalDescUniform->set(4);
    stateSet->addUniform(uIntervalDescUniform);

    // Add nurbs_u_intervals_data
    auto uIntervalDataBufferData = new BufferTemplate<decltype(serializedPartition.uIntervalsData)>;
    uIntervalDataBufferData->setData(serializedPartition.uIntervalsData);
    auto uIntervalDataBuffer = new TextureBuffer(uIntervalDataBufferData);
    uIntervalDataBuffer->setInternalFormat(GL_RGBA32I);
    stateSet->setTextureAttribute(5, uIntervalDataBuffer);

    auto uIntervalDataUniform = new Uniform(Uniform::Type::INT_SAMPLER_BUFFER, "nurbs_u_intervals_data", 1);
    uIntervalDataUniform->set(5);
    stateSet->addUniform(uIntervalDataUniform);

    // Add nurbs_curve_list
    auto curveListBufferData = new BufferTemplate<decltype(serializedPartition.trimmingCurveList)>;
    curveListBufferData->setData(serializedPartition.trimmingCurveList);
    auto curveListBuffer = new TextureBuffer(curveListBufferData);
    curveListBuffer->setInternalFormat(GL_RGBA32I);
    stateSet->setTextureAttribute(6, curveListBuffer);
    
    auto curveListUniform = new Uniform(Uniform::Type::SAMPLER_BUFFER, "nurbs_curve_list", 1);
    curveListUniform->set(6);
    stateSet->addUniform(curveListUniform);
}

}
}
