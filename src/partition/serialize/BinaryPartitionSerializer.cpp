#include "osgNurbs/partition/serialize/BinaryPartitionSerializer.h"

namespace LC
{

namespace
{

template <typename VEC3>
osg::Vec4 vec3ToVec4(VEC3 const& vec3)
{
    return osg::Vec4 { vec3[0], vec3[1], vec3[2], 0.0f };
}

}

////////////////////////////////////////////////////////////////////////////////
BinaryPartitionSerializer::SerializedPartition BinaryPartitionSerializer::serialize(BinaryPartitionSerializer::Partition const& binaryPartition)
{
    SerializedPartition serializedPartition;

    // Add the trimming curves
    for (auto const& curve: binaryPartition._curves) {
        serializedPartition.trimmingCurvesDesc.emplace_back(curve.order(), static_cast<int>(serializedPartition.trimmingCurvesData.size()), 0, 0);
        std::transform(curve.pointsBegin(), curve.pointsEnd(), std::back_inserter(serializedPartition.trimmingCurvesData), vec3ToVec4<Partition::BezierCurve::WPoint>);
    }

    const auto yPartEnd = binaryPartition.end();
    for (auto yPartIt = binaryPartition.begin(); yPartIt != yPartEnd; ++yPartIt)
    {
        std::size_t xIntervalBegin = serializedPartition.uIntervalsDesc.size();

        const auto xPartEnd = yPartIt->parts.end();
        for (auto xPartIt = yPartIt->parts.begin(); xPartIt != xPartEnd; ++xPartIt)
        {
            std::size_t curvesBegin = serializedPartition.trimmingCurveList.size();
            std::transform(xPartIt->curvesIndex.begin(), xPartIt->curvesIndex.end(),
                    std::back_inserter(serializedPartition.trimmingCurveList),
                    [&serializedPartition](std::size_t index){
                        return serializedPartition.trimmingCurvesDesc[index];
                    });
            std::size_t curvesEnd = serializedPartition.trimmingCurveList.size();

            // Compute the number of already classifiable intersections.
            std::set<std::size_t> intersectingCurves;
            {
                auto xPartCopy = xPartIt;
                ++xPartCopy;
                std::for_each(xPartCopy, xPartEnd, [&intersectingCurves](Partition::Partition const& part) {
                    intersectingCurves.insert(part.curvesIndex.begin(), part.curvesIndex.end());
                });
            }

            // Count the intersecting curves that are not in xPartIt->curvesIndex
            std::size_t numberOfIntersections = std::count_if(intersectingCurves.begin(), intersectingCurves.end(), [xPartIt](std::size_t index) {
                return xPartIt->curvesIndex.count(index) != 1;
            });

            serializedPartition.uIntervalsDesc.emplace_back(xPartIt->interval.low(), xPartIt->interval.high(), 0, 0);
            serializedPartition.uIntervalsData.emplace_back(static_cast<int>(curvesBegin), static_cast<int>(curvesEnd),
                                                            static_cast<int>(numberOfIntersections),
                                                            0);
        }

        std::size_t xIntervalEnd = serializedPartition.uIntervalsDesc.size();


        serializedPartition.vIntervalsDesc.emplace_back(yPartIt->interval.low(), yPartIt->interval.high(),
                                                        intAsFloatBits(static_cast<int>(xIntervalBegin)), intAsFloatBits(static_cast<int>(xIntervalEnd)));
    }

    return serializedPartition;
}

}
