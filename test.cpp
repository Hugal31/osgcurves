#include <array>
#include <iostream>
#include <random>

#include <osg/PositionAttitudeTransform>
#include <osg/ShapeDrawable>
#include <osg/Version>
#include <osgDB/Registry>
#include <osgViewer/Viewer>
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/TrackballManipulator>

#include <Core/EigenExtensions/EigenGeometryAndPlugins.h>
EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Vector4f)

#include "osgNurbs/bezier/Curve.h"
#include "osgNurbs/bezier/algorithms/Conversion.h"
#include "osgNurbs/bezier/algorithms/Monotonize.h"

#include "osgNurbs/partition/BinaryPartition.h"

#include "osgNurbs/SampledCurve.h"
#include "osgNurbs/TrimmedSurface.h"

namespace
{
osg::ref_ptr<osgGA::CameraManipulator> createCameraManipulator()
{
    osg::ref_ptr<osgGA::TrackballManipulator> cameraManipulator = new osgGA::TrackballManipulator;
    cameraManipulator->setAllowThrow(false);

    return cameraManipulator;
}

osgViewer::Viewer createViewer(osg::ref_ptr<osg::Node> const& rootNode)
{
    osgViewer::Viewer viewer;
    osg::ref_ptr<osgGA::CameraManipulator> cameraManipulator = createCameraManipulator();

    viewer.setSceneData(rootNode);
    viewer.setCameraManipulator(cameraManipulator);
    viewer.setUpViewInWindow(100, 100, 800, 600);
    viewer.addEventHandler(new osgViewer::StatsHandler);

    return viewer;
}

template <typename InputIterator>
std::vector<LC::bezier::Curve<float, 2> > monotonize(InputIterator curvesBegin,
                                                     InputIterator curvesEnd,
                                                     LC::bezier::Curve<float, 2>::Dimension dim)
{
    std::vector<LC::bezier::Curve<float, 2> > result;

    std::for_each(curvesBegin, curvesEnd, [&result, dim](LC::bezier::Curve<float, 2> const& curve) {
        LC::bezier::Monotonize<float>::monotonize(curve, dim, std::back_inserter(result));
    });

    return result;
}

/*
constexpr std::size_t heartOrder = 2;
std::array<typename LC::nurbs::Curve3f::HPoint, 7> heartCtrlPoints = {
        LC::nurbs::Curve3f::HPoint { 0, -1.5, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -5, 2, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -2, 5, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 0, 2, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 2, 5, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 5, 2, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 0, -1.5, 0, 1 },
};

std::array<float , 10> heartKnots = {
        0,
        0,
        0,
        0.4,
        0.5,
        0.5,
        0.6,
        1,
        1,
        1
};
 */
/*
std::array<typename LC::nurbs::Curve3f::HPoint, 9> heartCtrlPoints = {
        LC::nurbs::Curve3f::HPoint { 0, -4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, -4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, 0, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 0, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 4, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 4, 0, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 4, -4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 0, -4, 0, 1 },
};

std::array<float , 12> heartKnots = {
        0,
        0,
        0,
        0.25,
        0.25,
        0.5,
        0.5,
        0.75,
        0.75,
        1,
        1,
        1
};
*/

constexpr std::size_t heartOrder = 3;
std::array<typename LC::nurbs::Curve3f::HPoint, 8> heartCtrlPoints = {
        LC::nurbs::Curve3f::HPoint { -4, -4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 4, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 4, -4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, -4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 4, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, -4, 0, 1 },
};

std::array<float , 12> heartKnots = {
    0,
    0,
    0,
    0,
    0.364,
    0.455,
    0.545,
    0.636,
    1,
    1,
    1,
    1,
};

/*
constexpr std::size_t heartOrder = 2;

const std::array<typename LC::nurbs::Curve3f::HPoint, 5> heartCtrlPoints = {
        LC::nurbs::Curve3f::HPoint { -4, -4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 4, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { 4, -4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, 4, 0, 1 },
        LC::nurbs::Curve3f::HPoint { -4, -4, 0, 1 },
};

const std::array<float , 8> heartKnots = {
        0, 0, 0, 0.3, 0.7, 1, 1, 1
};
*/

}

int main()
{
    osg::ref_ptr<osg::PositionAttitudeTransform> rootNode = new osg::PositionAttitudeTransform;
    rootNode->setAttitude(osg::Quat { 0.7071068, 0, 0, 0.7071068 });

    // Perpare the NURBS trimming curve
    std::vector<typename LC::nurbs::Curve2f::HPoint> heartPoints;
    std::transform(std::begin(heartCtrlPoints), std::end(heartCtrlPoints), std::back_inserter(heartPoints), [](LC::nurbs::Curve3f::HPoint const& point) {
        return LC::nurbs::Curve2f::HPoint { (point.x() + 5.0f) / 10.0f, (point.y() + 5.0f) / 10.0f, point(3) };
    });

    LC::nurbs::Curve2f heartNurbsCurve {
        heartPoints,
        std::vector<float> { std::begin(heartKnots), std::end(heartKnots) },
        heartOrder
    };

    std::vector<LC::bezier::Curve<float, 2> > trimmingCurves;
    LC::CurveConverter<float, 2>::convert(heartNurbsCurve, std::back_inserter(trimmingCurves));

    trimmingCurves = monotonize(trimmingCurves.begin(), trimmingCurves.end(), LC::bezier::Curve<float, 2>::Dimension::x);
    trimmingCurves = monotonize(trimmingCurves.begin(), trimmingCurves.end(), LC::bezier::Curve<float, 2>::Dimension::y);

    LC::BinaryPartition<float> partition { trimmingCurves };

    for (auto partYIt = partition.begin(); partYIt != partition.end(); ++partYIt)
    {
        std::cout << "Y interval: [" << partYIt->interval.low() << ", " << partYIt->interval.high() << "]\n";
        for (auto partXIt = partYIt->parts.begin(); partXIt != partYIt->parts.end(); ++partXIt)
        {
            std::cout << "\tX interval: [" << partXIt->interval.low() << ", " << partXIt->interval.high() << "], " << partXIt->curvesIndex.size() << " curves \n";
        }
    }

    osg::ref_ptr<osg::nurbs::TrimmedSurface> trimmedSurface = new osg::nurbs::TrimmedSurface(trimmingCurves, partition);

    for (auto const& bezier: trimmingCurves)
    {
        std::cout << "Bezier of degree " << bezier.degree() << ", order " << bezier.order() << '\n';
        const auto end = bezier.pointsEnd();
        for (auto it = bezier.pointsBegin(); it != end; ++it)
        {
            std::cout << "\t(" << it->operator[](0) << ", " << it->operator[](1) << ", " << it->operator[](2) /*<< ", " << it->operator[](3)*/ << ")\n";
        }
    }
    std::cout << std::flush;

    // Show the bezier curves.
    std::uniform_real_distribution<float> rng (0.1f, 1.0f);
    std::default_random_engine rngEngine {};
    osg::ref_ptr<osg::PositionAttitudeTransform> bezierGroup = new osg::PositionAttitudeTransform;
    bezierGroup->setPosition(osg::Vec3d { 0, 0, 0.01f });
    for (LC::bezier::Curve<float, 2> const& bezier: trimmingCurves)
    {
        std::vector<typename LC::bezier::Curve<float, 3>::WPoint> ctrlPoints (bezier.order());
        std::transform(bezier.pointsBegin(), bezier.pointsEnd(), ctrlPoints.begin(), [](LC::bezier::Curve<float, 2>::WPoint const& point) {
            return LC::bezier::Curve<float, 3>::WPoint { point.x(), point.y(), 0.0, point.z() };
        });
        osg::ref_ptr<osg::Vec4Array> colors = new osg::Vec4Array;
        colors->push_back(osg::Vec4(rng(rngEngine), rng(rngEngine), rng(rngEngine), 1.0f));
        colors->setBinding(osg::Array::BIND_OVERALL);
        auto *curve = new osg::nurbs::SampledCurve<LC::bezier::Curve<float, 3> >(LC::bezier::Curve<float, 3> { ctrlPoints.begin(), ctrlPoints.end() });
        curve->setColorArray(colors);

        bezierGroup->addChild(curve);
    }

    rootNode->addChild(trimmedSurface);
    rootNode->addChild(bezierGroup);

    osgViewer::Viewer viewer = createViewer(rootNode);
    return viewer.run();
}
