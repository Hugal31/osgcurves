#pragma once

#include <osg/Geometry>

namespace osg
{

namespace nurbs
{

/**
 * @brief 3D Curve for display
 */
template <typename _CurveDef>
class SampledCurve: public osg::Geometry
{
public:
    using CurveDef = _CurveDef;

public:
    explicit SampledCurve(CurveDef curve, unsigned int nSamplePerControlPoint = 8);
    ~SampledCurve() override = default;

    SampledCurve(const SampledCurve &other, const CopyOp& copyop = osg::CopyOp::SHALLOW_COPY);

    SampledCurve &operator=(const SampledCurve &other) = delete;
    SampledCurve &operator=(SampledCurve &&other) = delete;

protected:
    void initializeCurve();

protected:
    /// @brief Definition of the curve.
    CurveDef _curve;
    /// @brief Number of sample of the curve.
    unsigned int _nSamples;
};

} // namespace nurbs

} // namespace osg

#include "SampledCurve.inl"
