#pragma once

#include "Curve.h"
#include "algorithms/CurveEvaluator.h"
#include "algorithms/Helpers.h"
#include "algorithms/Horner.h"

namespace LC
{

namespace bezier
{

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
Curve<_Scalar, _Dimension>::Curve(WPoints points)
        : _points { std::move(points) }
{}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
template <class WPointInIterator>
Curve<_Scalar, _Dimension>::Curve(WPointInIterator pointsBegin, WPointInIterator pointsEnd)
        : Curve { { pointsBegin, pointsEnd } }
{}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, std::size_t _Dimension>
typename Curve<_Scalar, _Dimension>::Point Curve<_Scalar, _Dimension>::pointAt(_Scalar u, CurveEvaluator<WPoint> const& evaluator) const
{
    return evaluator(pointsBegin(), pointsEnd(), u).template head<_Dimension>();
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, std::size_t _Dimension>
bool Curve<_Scalar, _Dimension>::isMonotonic(Dimension dim) const
{
    std::set<Scalar> extremas;
    findExtremas(std::inserter(extremas, extremas.end()), dim);

    if (extremas.empty())
        return true;
    else
        return extremas.size() == 1 && (*extremas.begin() == Scalar(0) || *extremas.begin() == Scalar(1));
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, std::size_t _Dimension>
bool Curve<_Scalar, _Dimension>::isWeakMonotonic(Dimension dim) const
{
    if (order() <= 2)
        return true;
    else {
        bool increasing = true,
                decreasing = true;

        WPointsConstIterator end = pointsEnd();
        for (WPointsConstIterator it = pointsBegin(), second = pointsBegin() + 1; second != end; ++it, ++second)
        {
            increasing &= it->operator[](dim) <= second->operator[](dim);
            decreasing &= it->operator[](dim) >= second->operator[](dim);
        }

        return increasing || decreasing;
    }
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
template <typename OutputIterator>
void Curve<_Scalar, _Dimension>::findExtremas(OutputIterator rootsInserter, Dimension dim, std::size_t iterations) const
{
    if (isWeakMonotonic(dim))
        return;

    Curve hodo = isRational() ? scaledHodograph() : hodograph();

    Curve<float, 2> hodo2D = hodo.nishita(dim);

    std::vector<Curve<float, 2> > nthDerivativeCurves { { hodo2D } };

    for (std::size_t i = 1; i < hodo.degree(); ++i)
        nthDerivativeCurves.emplace_back(nthDerivativeCurves.back().hodograph().nishita(Dimension::y));

    Scalar uMin = 0;
    Scalar uMax = 1;

    std::vector<Interval<Scalar> > intervals {{{ uMin, uMax }}};

    for (int i = static_cast<int>(nthDerivativeCurves.size()) - 1; i >= 0; --i) {
        Scalar lastRoot = uMin;

        std::vector<Interval<Scalar> > newIntervals;

        for (std::size_t v = 0; v < intervals.size(); ++v) {
            Scalar root = 0;
            bool isRoot = nthDerivativeCurves[i].bisect(Dimension::y, 0.0, root, intervals[v], iterations);

            if (isRoot) {
                newIntervals.emplace_back(lastRoot, root);
                lastRoot = root;
            }

            // Roots of the final curve
            if (isRoot && i == 0) {
                *rootsInserter = root;
            }
        }

        newIntervals.emplace_back(lastRoot, uMax);
        intervals = std::move(newIntervals);
    }
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
Curve<_Scalar, _Dimension> Curve<_Scalar, _Dimension>::hodograph() const
{
    WPoints ctrlPoints;
    ctrlPoints.reserve(order() - 1);
    const Scalar deg = degree();

    WPointsConstIterator end = pointsEnd();
    for (WPointsConstIterator it = pointsBegin(), second = pointsBegin() + 1; second != end; ++it, ++second) {
        WPoint cp = deg * (*second - *it);
        cp.reverse()[0] = 1;
        ctrlPoints.push_back(cp);
    }

    return Curve(std::move(ctrlPoints));
}

namespace
{
inline std::size_t factorial(std::size_t n)
{
    std::size_t fac = 1;

    for (std::size_t i = 2; i <= n; ++i)
        fac *= i;

    return fac;
}

inline std::size_t binomialCoefficient(std::size_t n, std::size_t i)
{
    return i > n ? 0 : factorial(n) / (factorial(n - i) * factorial(i));
}
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
Curve<_Scalar, _Dimension> Curve<_Scalar, _Dimension>::scaledHodograph() const
{
    std::size_t deg = degree();

    WPoint zeroPoint = WPoint::Zero();
    zeroPoint.reverse()[0] = Scalar(1);
    WPoints ctrlPoints { 2 * deg - 1, zeroPoint };

    for (std::size_t k = 0; k <= 2 * deg - 2; ++k) {
        std::size_t e_idx = k / 2;
        std::size_t s_idx = std::max(0, int(k) - int(deg) + 1);

        for (std::size_t i = s_idx; i <= e_idx ;++i) {
            Scalar alpha = Scalar(k - 2 * i + 1) * Scalar(binomialCoefficient(deg, i)) * Scalar(binomialCoefficient(deg, k - i + 1));
            alpha = alpha / Scalar(binomialCoefficient(2 * deg - 2, k));

            WPoint D_ik = (_points[i].reverse()[0] * _points[k-i+1].reverse()[0]) *
                           (_points[k-i+1] - _points[i]);
            ctrlPoints[k] = ctrlPoints[k] + alpha * D_ik;
        }
    }

    // set weights to 1
    std::for_each(ctrlPoints.begin(), ctrlPoints.end(), [](WPoint& p) { p.reverse()[0] = Scalar(1); });

    return Curve(std::move(ctrlPoints));
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
Curve<_Scalar, 2> Curve<_Scalar, _Dimension>::nishita(Dimension dim) const
{
    typename Curve<_Scalar, 2>::WPoints ctrlPoints;

    const Scalar deg = degree();
    for (std::size_t i = 0; i < _points.size(); ++i) {
        typename Curve<_Scalar, 2>::WPoint equidistantControlPoint { Scalar(i) / deg,
                                                                     _points[i][dim],
                                                                     _points[i].reverse()[0] };
        ctrlPoints.push_back(equidistantControlPoint);
    }

    return Curve<Scalar, 2>(std::move(ctrlPoints));
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
bool Curve<_Scalar, _Dimension>::isRational() const
{
    return std::any_of(pointsBegin(), pointsEnd(), [](WPoint const& point) {
        return point.reverse()[0] != Scalar(1);
    });
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, std::size_t _Dimension>
void Curve<_Scalar, _Dimension>::clipLeft(Scalar u)
{
    assert(u >= Scalar(0) && u <= Scalar(1));

    // Switch to homogenous points
    std::transform(_points.begin(), _points.end(), _points.begin(), toHomogenous<WPoint>);

    Curve<_Scalar, _Dimension> helper = *this;

    const std::size_t deg = degree();
    for (std::size_t i = 0; i <= deg; ++i) {
        for (std::size_t j = 0; j < deg - i; ++j) {
            _points[j] = (Scalar(1) - u) * _points[j] + u * _points[j + 1];
        }
        if (i < deg)
            helper._points[deg - i] = _points[deg - i];
    }

    helper._points[0] = _points[0];
    // Get helper points back into *this, in euclidian space.
    // Equivalent to
    //     *this = helper
    //     // std::transform(_points.begin(), ...
    std::transform(helper.pointsBegin(), helper.pointsEnd(), _points.begin(), toEuclidian<WPoint>);
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, std::size_t _Dimension>
void Curve<_Scalar, _Dimension>::clipRight(Scalar u)
{
    assert(u >= Scalar(0) && u <= Scalar(1));

    // Switch to homogenous points
    std::transform(_points.begin(), _points.end(), _points.begin(), toHomogenous<WPoint>);

    Curve helper = *this;

    const std::size_t deg = degree();
    for (std::size_t i = 0; i <= deg; ++i) {
        for (std::size_t j = 0; j < deg - i; ++j) {
            _points[j] = (Scalar(1) - u) * _points[j] + u * _points[j + 1];
        }
        if (i < deg)
            helper._points[i + 1] = _points[0];
    }

    std::swap(*this, helper);

    // Switch back to euclidian points
    std::transform(_points.begin(), _points.end(), _points.begin(), toEuclidian<WPoint>);
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, std::size_t _Dimension>
void Curve<_Scalar, _Dimension>::clipBoth(Scalar uLeft, Scalar uRight)
{
    if (uLeft > uRight)
        std::swap(uLeft, uRight);

    if (uLeft == Scalar(0) && uRight == Scalar(1))
        return;

    clipLeft(uLeft);
    clipRight((uRight - uLeft) / (Scalar(1) - uLeft));
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, std::size_t _Dimension>
std::pair<Curve<_Scalar, _Dimension>, Curve<_Scalar, _Dimension> > Curve<_Scalar, _Dimension>::split(Scalar u) const
{
    std::pair<Curve<_Scalar, _Dimension>, Curve<_Scalar, _Dimension> > result = { *this, *this };

    result.first.clipRight(u);
    result.second.clipLeft(u);

    // assert continuity
    result.first.back() = result.second.front();

    return result;
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
template <typename Iterator, typename OutputIterator>
void Curve<_Scalar, _Dimension>::split(Iterator splitsBegin, Iterator splitsEnd, OutputIterator curveInserter) const
{
    std::size_t nSplit = std::distance(splitsBegin, splitsEnd);

    // Handle special cases
    if (nSplit == 0) {
        *curveInserter = *this;
        return;
    } else if (nSplit == 1) {
        auto splitted = split(*splitsBegin);
        *curveInserter = std::move(splitted.first);
        *curveInserter = std::move(splitted.second);

        return;
    }

    // Copy the value to access to a operator++-able Iterator (TODO: Avoid this)

    std::vector<Scalar> splits { splitsBegin, splitsEnd };
    WPoint connection;

    // First segment
    {
        Curve<_Scalar, _Dimension> tmp = *this;
        tmp.clipRight(splits.front());
        connection = tmp.back();
        *curveInserter = tmp;
    }

    // Inner segments
    auto end = splits.end();
    for (auto it = splits.begin() + 1; it + 1 < end; ++it)
    {
        Curve<_Scalar, _Dimension> tmp = *this;
        tmp.clipBoth(*it, *(it + 1));
        tmp._points[0] = connection;
        connection  = tmp.back();
        *curveInserter = tmp;
    }


    // Last segment
    {
        Curve<_Scalar, _Dimension> tmp = *this;
        tmp.clipLeft(splits.back());
        tmp._points[0] = connection;
        *curveInserter = tmp;
    }
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, std::size_t _Dimension>
bool Curve<_Scalar, _Dimension>::bisect(Dimension dim,
                                        Scalar dimensionValue,
                                        Scalar &rootParameter,
                                        Interval<Scalar> range,
                                        std::size_t maxIterations) const
{
    // Test if the root is at the extremas
    if (_points.front()[dim] == dimensionValue && range.low() == Scalar(0)) {
        rootParameter = 0;
        return true;
    } else if (_points.back()[dim] == dimensionValue && range.high() == Scalar(1)) {
        rootParameter = 1;
        return true;
    }

    Scalar uMin = range.low(), uMax = range.high();

    const HornerCurveEvaluator<WPoint> evaluator;
    WPoint pMin = evaluator(pointsBegin(), pointsEnd(), uMin),
            pMax = evaluator(pointsBegin(), pointsEnd(), uMax);

    bool intersectsIncreasing = (pMin[dim] < dimensionValue) && (pMax[dim] > dimensionValue);
    bool intersectsDecreasing = (pMin[dim] > dimensionValue) && (pMax[dim] < dimensionValue);

    if (intersectsIncreasing || intersectsDecreasing)
    {
        // There must be a root
        for (std::size_t i = 0; i <= maxIterations; ++i) {
            rootParameter = (uMin + uMax) / Scalar(2);
            WPoint pi = evaluator(pointsBegin(), pointsEnd(), rootParameter);

            if (intersectsIncreasing) {
                if (pi[dim] > dimensionValue)
                    uMax = rootParameter;
                else
                    uMin = rootParameter;
            } else {
                if (pi[dim] > dimensionValue)
                    uMin = rootParameter;
                else
                    uMax = rootParameter;
            }
        }

        rootParameter = (uMin + uMax) / Scalar(2);
        return true;
    }

    return false;
}

} // namespace bezier.

} // namespace LC.
