#pragma once

#include <Core/EigenExtensions/EigenGeometryAndPlugins.h>

/**
 * @brief Project a point to the homogenous space, i.e. multiply all its components by the last one.
 * @tparam Point Must be an Eigen Vector, or equivalent
 * @todo Move to core after
 */
template <typename Point>
constexpr Point toHomogenous(Point const& point)
{
    Point res = point;
    res.template head<Point::SizeAtCompileTime - 1>() *= res.reverse()[0];
    return res;
}

/**
 * @brief Project a point to the euclidan space, i.e. divide all its components by the last one.
 * @tparam Point Must be an Eigen Vector, or equivalent
 * @todo Move to core after
 */
template <typename Point>
constexpr Point toEuclidian(Point const& point)
{
    Point res = point;
    res.template head<Point::SizeAtCompileTime - 1>() /= res.reverse()[0];
    return res;
}
