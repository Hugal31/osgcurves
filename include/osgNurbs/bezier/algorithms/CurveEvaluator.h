#pragma once

#include "Core/EigenExtensions/EigenGeometryAndPlugins.h"

namespace LC
{

namespace bezier
{

/**
 * @brief Interface for evaluate curve.
 * @see LC::bezier::Curve for usage
 */
template <typename _Point>
class CurveEvaluator
{
public:
    using Point = _Point;
    using Scalar = typename Point::Scalar;
    using ConstPointIterator = typename std::vector<Point, Eigen::aligned_allocator<Point> >::const_iterator;

public:
    virtual ~CurveEvaluator() = default;

    /**
     * @brief Evaluate a bezier curve
     * @param pointsBegin Iterator over the control points of the curve, in euclidian space.
     * @param u Point to evaluate. Must be in [0 and 1].
     * @return The point of the curve, in euclidian space.
     */
    virtual Point operator()(ConstPointIterator pointsBegin,
                             ConstPointIterator pointsEnd,
                             Scalar u) const = 0;
};

}

}
