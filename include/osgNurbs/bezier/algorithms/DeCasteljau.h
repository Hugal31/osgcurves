#pragma once

#include <cstddef>

#include "CurveEvaluator.h"
#include "Helpers.h"

namespace LC
{

namespace bezier
{

template <typename _Point>
class DeCasteljauCurveEvaluator final: public CurveEvaluator<_Point>
{
public:
    using Point = typename CurveEvaluator<_Point>::Point;
    using Scalar = typename CurveEvaluator<_Point>::Scalar;
    using ConstPointIterator = typename CurveEvaluator<_Point>::ConstPointIterator ;

public:
    ~DeCasteljauCurveEvaluator() = default;

    Point operator()(ConstPointIterator pointsBegin,
                     ConstPointIterator pointsEnd,
                   Scalar u) const override
    {
        const std::size_t order = std::distance(pointsBegin, pointsEnd);
        assert(order > 0);

        // Handle special cases
        if (u == Scalar(0))
            return *pointsBegin;

        if (u == Scalar(1))
            return *(pointsEnd - 1);

        std::vector<Point> tmp(order);
        std::transform(pointsBegin, pointsEnd, tmp.begin(), &toHomogenous<Point>);

        // evaluate using decasteljau scheme
        for (std::size_t i = 0; i < order - 1; ++i)
        {
            for (std::size_t j = 0; j < order - 1 - i; ++j)
            {
                tmp[j] = (Scalar(1) - u) * tmp[j] + u * tmp[j + 1];
            }
        }

        return toEuclidian(tmp[0]);
    }
};

}

}

