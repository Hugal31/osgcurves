#pragma once

#include <set>
#include <stdexcept>
#include <vector>

#include "Core/EigenExtensions/EigenGeometryAndPlugins.h"
#include "Core/Nurbs/Curve.h"

#include "../Curve.h"

namespace LC
{
/**
 * @todo Move to another path or put in namespace bezier ?
 */
template<typename _Scalar, int _Dimension>
class CurveConverter
{
public:
    using Point = typename bezier::Curve<_Scalar, _Dimension>::WPoint;
    using Scalar = _Scalar;

public:
    /**
     * @brief Convert a nurbs curve to a bezier by knot insertion
     * @param nc Curve to convert
     * @tparam InsertIterator An inserter iterator over bezier::Curve<Scalar, _Dimension>.
     */
    template <typename InsertIterator>
    static void convert(nurbs::Curve<Scalar, _Dimension> const &nc, InsertIterator inserter);

private:
    template <typename _PointsContainer>
    static void knotInsertion(_PointsContainer &points, std::multiset<Scalar> &knots, std::size_t order);
    template <typename _PointsContainer>
    static void knotInsertion(_PointsContainer &points, std::multiset<Scalar> &knots, std::size_t order, Scalar t);
};
}

namespace LC
{

////////////////////////////////////////////////////////////////////////////////
/// From https://github.com/scholli/gpucast (gpucast_math/include/gpucast/math/parametric/algorithm/converter_impl.hpp)
template <typename _Scalar, int _Dimension>
template <typename InsertIterator>
void CurveConverter<_Scalar, _Dimension>::convert(nurbs::Curve<_Scalar, _Dimension> const &nc,
                                                  InsertIterator inserter)
{
    std::vector<Point> ctrlPoints { nc.ctrlPts() };
    std::multiset<Scalar> knots { nc.knots().begin(), nc.knots().end() };

    const std::size_t degree = nc.degree();
    const std::size_t order = degree + 1;

    if (order <= 1)
        throw std::domain_error("Irregular NURBS order");

    if (!nc.isValid())
        throw std::invalid_argument("Invalid NURBS curve");

    // Perform the knot insertion
    knotInsertion(ctrlPoints, knots, order);

    // TODO Know when this happen
    if (ctrlPoints.size() + order != knots.size())
        throw std::runtime_error("Knot insertion failed");

    // Compute how many bezier curve will result
    const std::size_t parts = (knots.size() - 2 * order) / degree + 1;

    typename std::vector<Point>::iterator start = std::begin(ctrlPoints);
    typename std::vector<Point>::iterator end = std::begin(ctrlPoints);
    std::advance(end, order);

    for (std::size_t i = 0; i < parts; ++i)
    {
        *inserter = bezier::Curve<Scalar, _Dimension> { start, end };

        std::advance(start, degree);
        std::advance(end, degree);
    }
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, int _Dimension>
template <typename _PointsContainer>
void CurveConverter<_Scalar, _Dimension>::knotInsertion(_PointsContainer &points, std::multiset<Scalar> &knots, std::size_t order)
{
    std::set<Scalar> uniqueKnots { knots.begin(), knots.end() };

    for (Scalar knot: uniqueKnots)
    {
        if (knots.count(knot) < order - 1)
            knotInsertion(points, knots, order, knot);
    }
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar, int _Dimension>
template <typename _PointsContainer>
void CurveConverter<_Scalar, _Dimension>::knotInsertion(_PointsContainer &points, std::multiset<Scalar> &knots, std::size_t order, Scalar t)
{
    std::vector<Scalar> kv_cpy { knots.begin(), knots.end() };
    const std::size_t p = order - 1;
    const std::size_t s = knots.count(t);
    const std::size_t r = std::max(std::size_t(0), p - s);

    // get knotspan
    const std::size_t k = std::distance(knots.begin(), knots.upper_bound(t));
    const std::size_t nPoints = points.size();

    const std::size_t nq = nPoints + r;

    std::vector<Point> qw { nq },
            rw { p - s + 1 };

    // TODO Assert this is correct

    // Copy unaffected points and transform into homogenous coords
    std::transform(std::begin(points), std::begin(points) + k - p + 1, std::begin(qw), &toHomogenous<Point>);
    std::transform(std::begin(points) + k - s - 1, std::end(points), std::begin(qw) + k - s - 1 + r, &toHomogenous<Point>);

    // Helper points
    // TODO: Use to std::transform?
    for (std::size_t i = 0; i <= p - s; ++i)
        rw[i] = toHomogenous<Point>(points[k - p + i - 1]);

    std::size_t L;
    for (std::size_t j = 1; j <= r; ++j)
    {
        L = k - p + j;
        for (std::size_t i = 0; i <= p - j - s; ++i)
        {
            Scalar alpha = (t - kv_cpy[L + i - 1]) / (kv_cpy[i + k] - kv_cpy[L + i - 1]);
            rw[i] = alpha * rw[i + 1] + (Scalar(1) - alpha) * rw[i];
        }

        qw[L - 1] = rw[0];
        qw[k + r - j - s - 1] = rw[p - j - s];
    }

    for (std::size_t i = 0; i < r; ++i)
        knots.insert(t);

    points.clear();

    std::transform(std::begin(qw), std::end(qw), std::back_inserter(points), &toEuclidian<Point>);
}

}
