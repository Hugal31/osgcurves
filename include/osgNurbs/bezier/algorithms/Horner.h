#pragma once

#include "CurveEvaluator.h"
#include "Helpers.h"


namespace LC
{

namespace bezier
{

template <typename _Point>
class HornerCurveEvaluator final: public CurveEvaluator<_Point>
{
public:
    using Point = typename CurveEvaluator<_Point>::Point;
    using Scalar = typename CurveEvaluator<_Point>::Scalar;
    using ConstPointIterator = typename CurveEvaluator<_Point>::ConstPointIterator ;

public:
    ~HornerCurveEvaluator() = default;

    Point operator()(ConstPointIterator pointsBegin,
                     ConstPointIterator pointsEnd,
                     Scalar u) const override
    {
        const std::size_t order = std::distance(pointsBegin, pointsEnd);
        const Scalar oneMinusU = Scalar(1) - u;
        Scalar bc = 1, tn = 1;

        Point tmp = oneMinusU * toHomogenous(*pointsBegin);

        for (std::size_t i = 1; i < order - 1; ++i) {
            tn *= u;
            bc *= Scalar(order - i) / Scalar(i);

            ++pointsBegin;
            tmp = oneMinusU * (tmp + tn * bc * toHomogenous(*pointsBegin));
        }

        ++pointsBegin;

        return toEuclidian<Point>(tmp + tn * u * toHomogenous(*pointsBegin));
    }

};

} // namespace bezier.

} // namespace LC.
