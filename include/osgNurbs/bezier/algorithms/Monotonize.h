#pragma once

#include <set>

#include "osgNurbs/bezier/Curve.h"

namespace LC
{
namespace bezier
{

/**
 * @brief Set of function to monotonize bezier curve.
 * @note For now, only works with 2D curve. Monotonize over u
 */
template<typename _Scalar>
class Monotonize
{
public:
    using Scalar = _Scalar;

public:
    template <typename InsertIterator>
    static void monotonize(Curve<Scalar, 2> const& bezierCurve, typename Curve<Scalar, 2>::Dimension dim, InsertIterator curveInserter);

private:
    template <typename Iterator>
    static void correctDerivativeAtSplitPoints(Iterator begin, Iterator end, typename Curve<Scalar, 2>::Dimension dim);
};

////////////////////////////////////////////////////////////////////////////////
// From https://github.com/scholli/gpucast (gpucast_math/include/math/parametric/implementation/beziercurve_impl.hpp)
template<typename _Scalar>
template<typename InsertIterator>
void Monotonize<_Scalar>::monotonize(Curve<Scalar, 2> const& bezierCurve, typename Curve<Scalar, 2>::Dimension dim, InsertIterator curveInserter)
{
    std::vector<Curve<Scalar, 2> > input = { bezierCurve };
    std::vector<Curve<Scalar, 2> > output;

    bool continueSplitting = true;
    while (continueSplitting) {
        continueSplitting = false;

        for (Curve<Scalar, 2> const& curve: input) {
            std::set<Scalar> extremas;
            curve.findExtremas(std::inserter(extremas, extremas.end()), dim);

            if (extremas.empty()) {
                *curveInserter = curve;
            } else {
                std::vector<Curve<Scalar, 2> > splittedCurves;
                curve.split(extremas.begin(), extremas.end(), std::back_inserter(splittedCurves));
                correctDerivativeAtSplitPoints(splittedCurves.begin(), splittedCurves.end(), dim);

                for (Curve<Scalar, 2> const& splittedCurve: splittedCurves) {
                    if (splittedCurve.isMonotonic(dim)) {
                        *curveInserter = splittedCurve;
                    } else {
                        continueSplitting = true;
                        output.push_back(splittedCurve);
                    }
                }
            }
        }

        input = output;
    }
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar>
template <typename Iterator>
void Monotonize<_Scalar>::correctDerivativeAtSplitPoints(Iterator begin, Iterator end, typename Curve<Scalar, 2>::Dimension dim)
{
    // make sure neighboring curves have same value for dim after split
    for (Iterator second = begin + 1; second != end; ++begin, ++second) {
        std::size_t degree = begin->degree();
        Scalar extremaValue = begin->operator[](degree)[dim];

        begin->operator[](degree - 1)[dim] = extremaValue;
        second->operator[](0)[dim] = extremaValue;
        second->operator[](1)[dim] = extremaValue;
    }
}

} // namespace bezier.

} // namespace LC.
