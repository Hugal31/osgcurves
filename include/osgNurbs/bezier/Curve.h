#pragma once

#include <stdexcept>
#include <utility>
#include <vector>

#include <Core/EigenExtensions/EigenGeometryAndPlugins.h>

#include "algorithms/DeCasteljau.h"

#include "../partition/Interval.h"

namespace LC
{

namespace bezier
{

/**
 * @node Mainly inspired from https://github.com/scholli/gpucast
 * @tparam _Scalar Read type
 * @tparam _Dimension Number of dimension in which the bezier is defined
 */
template <typename _Scalar, std::size_t _Dimension>
class Curve
{
public:
    using Scalar = _Scalar;

    /// \brief "Weighted" point. The last scalar is the weight of the point.
    using WPoint = Eigen::Matrix<_Scalar, _Dimension + 1, 1>;
    using Point = Eigen::Matrix<_Scalar, _Dimension, 1>;

    using WPoints = typename std::vector<WPoint, Eigen::aligned_allocator<WPoint> >;
    using WPointsConstIterator = typename WPoints::const_iterator;

    enum Dimension: int {
        x = 0,
        y,
        z
    };

public:
    /**
     * @brief Construct the curve with two vectors
     * @param points Control points of the Bezier curve
     */
    explicit Curve(WPoints points);
    /**
     * @brief Construct the curve with iterators
     * @tparam PointInIterator Must be an InputIterator over WPoint
     */
    template <class WPointInIterator>
    Curve(WPointInIterator pointsBegin, WPointInIterator pointsEnd);
    Curve(Curve const& other) = default;
    Curve(Curve && other) noexcept = default;
    ~Curve() = default;

    Curve &operator=(Curve const& other) = default;
    Curve &operator=(Curve && other) noexcept = default;

    /// \name Accessors
    ///@{
    std::size_t order() const { return _points.size(); }
    std::size_t degree() const { return order() - 1; }

    WPointsConstIterator pointsBegin() const { return _points.begin(); }
    WPointsConstIterator pointsEnd() const { return _points.end(); }

    WPoint const& front() const { return _points.front(); }
    WPoint const& back() const { return _points.back(); }
    WPoint const& operator[](std::size_t index) const { return _points[index]; }

    WPoint& front() { return _points.front(); }
    WPoint& back() { return _points.back(); }
    WPoint& operator[](std::size_t index) { return _points[index]; }

    /// @returns true if the curve is monotonic over dimension.
    bool isMonotonic(Dimension dimension) const;

    /// @returns true if not all the weights of the control points are 1.
    bool isRational() const;


    /**
     * @brief Do a bisection within a range to find a root
     * @param dim Dimension to search the root
     * @param directionValue Value of the curve within the dimension
     * @param[out] rootParameter The variable to store the found root U parameter.
     * @param range
     * @param maxIterations
     * @return true if a root was found.
     */
    bool bisect(Dimension dim,
                Scalar directionValue,
                Scalar &rootParameter,
                Interval<Scalar> range = {0, 1},
                std::size_t maxIterations = 64) const;

    ///@}

    /// \name Diverse operations
    ///@{

    /**
     * @brief Find the extremas over x
     * @tparam OutputIterator Satisfies OutputIterator for Scalar
     * @param rootsInserter OutputIterator for finded roots.
     */
    template <typename OutputIterator>
    void findExtremas(OutputIterator rootsInserter, Dimension dim, std::size_t iterations = 64) const;

    /// @brief Clip the left side of the curve to its value on u.
    void clipLeft(Scalar u);

    /// @brief Clip the right side of the curve to its value on u.
    void clipRight(Scalar u);

    /// @brief Clip the both sides of the curve to its value on uLeft and uRight.
    void clipBoth(Scalar uLeft, Scalar uRight);

    /**
     * @brief Split the curve over u
     * @param u Value where to split the curve. Must int [0, 1]
     * @return The two splitted curves.
     */
    std::pair<Curve<_Scalar, _Dimension>, Curve<_Scalar, _Dimension> > split(Scalar u) const;

    /**
     * @brief Split the curve over the u in the [splitsBegin - splitsEnd) range.
     * @tparam Iterator Satisfies "ForwardIterator" over Scalar
     * @tparam OutputIterator Satisfies the "OutputIterator" requirements over Curve.
     * @param splitsBegin Begin of the range of the split U values. Must be between [0, 1],
     * @param splitsEnd End of the range of the split U values.
     * @param curveInserter Inserter to output the splitted curves.
     */
    template <typename Iterator, typename OutputIterator>
    void split(Iterator splitsBegin, Iterator splitsEnd, OutputIterator curveInserter) const;
    ///@}

    /// \brief Evaluate the curve
    Point pointAt(Scalar u, CurveEvaluator<WPoint> const& evaluator = DeCasteljauCurveEvaluator<WPoint>{}) const;

    Point operator()(Scalar u, CurveEvaluator<WPoint> const& evaluator = DeCasteljauCurveEvaluator<WPoint>{}) const { return pointAt(u, evaluator); };

private:
    /**
     * @brief Easy test cases for monotonic
     * @returns true if the degree of the curve is one, or if all the control points are either in increasing order or decresing over dim.
     */
    bool isWeakMonotonic(Dimension dim) const;

    /// @warning Only valid if !isRational()
    Curve hodograph() const;
    Curve scaledHodograph() const;

    /// @return a 2D bezier curve over dim axis
    Curve<Scalar, 2> nishita(Dimension dim) const;

private:
    WPoints _points;
};

}

}

#include "Curve.inl"
