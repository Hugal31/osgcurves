#pragma once

#include <osg/Geometry>

#include "bezier/Curve.h"
#include "partition/BinaryPartition.h"

namespace osg
{
namespace nurbs
{

/**
 * @brief Attempt to make a trimmed NURBS Surface
 * @note For now this is simply a flat square surface
 */
class TrimmedSurface: public Geometry
{
public:
    using CurveDef = LC::bezier::Curve<float, 2>;

public:
    TrimmedSurface(std::vector<CurveDef> trimmingCurves, LC::BinaryPartition<float> partition);

    ~TrimmedSurface() override = default;

    TrimmedSurface(const TrimmedSurface &other, const CopyOp& copyop = osg::CopyOp::SHALLOW_COPY);

    TrimmedSurface &operator=(const TrimmedSurface &other) = delete;
    TrimmedSurface &operator=(TrimmedSurface &&other) = delete;

protected:
    void initializeSurface();

protected:
    std::vector<CurveDef> _trimmingCurves;
    LC::BinaryPartition<float> _partition;
};

} // namespace osg.

} // namespace nurbs.
