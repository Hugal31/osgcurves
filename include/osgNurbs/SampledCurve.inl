#include "osgNurbs/SampledCurve.h"

namespace osg
{
namespace nurbs
{

namespace
{

////////////////////////////////////////////////////////////////////////////////
template<typename _CurveDef>
constexpr float getCurveBegin(_CurveDef const&)
{
    return 0.0;
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, int _Dimension>
constexpr float getCurveBegin(LC::nurbs::Curve<_Scalar, _Dimension> const &curveDef)
{
    return curveDef.minKnot();
}

////////////////////////////////////////////////////////////////////////////////
template<typename _CurveDef>
constexpr float getCurveEnd(_CurveDef const&)
{
    return 1.0;
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, int _Dimension>
constexpr float getCurveEnd(LC::nurbs::Curve<_Scalar, _Dimension> const &curveDef)
{
    return curveDef.maxKnot();
}

////////////////////////////////////////////////////////////////////////////////
template<typename _CurveDef>
constexpr std::size_t getNumberOfControlPoints(_CurveDef const& curveDef)
{
    return curveDef.order();
}

////////////////////////////////////////////////////////////////////////////////
template<typename _Scalar, int _Dimension>
constexpr std::size_t getNumberOfControlPoints(LC::nurbs::Curve<_Scalar, _Dimension> const& curveDef)
{
    return curveDef.ctrlPts().size();
}

} // namespace.

////////////////////////////////////////////////////////////////////////////////
template<typename _CurveDef>
SampledCurve<_CurveDef>::SampledCurve(CurveDef curve, unsigned int nSamplePerControlPoint)
        : _curve(std::move(curve)), _nSamples(static_cast<unsigned int>(_curve.degree() + 1) * nSamplePerControlPoint)
{
    initializeCurve();
}

////////////////////////////////////////////////////////////////////////////////
template<typename _CurveDef>
SampledCurve<_CurveDef>::SampledCurve(const SampledCurve &other, const CopyOp &copyop)
        : Geometry(other, copyop), _curve(other._curve), _nSamples(other._nSamples)
{
}

////////////////////////////////////////////////////////////////////////////////
template<typename _CurveDef>
void SampledCurve<_CurveDef>::initializeCurve()
{
    auto *points = new osg::Vec3Array;

    const float minKnot = getCurveBegin(_curve),
            maxKnot = getCurveEnd(_curve);
    for (unsigned int i = 0; i <= _nSamples; ++i)
    {
        float u = i / static_cast<float>(_nSamples) * (maxKnot - minKnot) + minKnot;
        Eigen::Matrix<float, 3, 1> p = _curve(u);

        points->push_back(Vec3(p.x(), p.y(), p.z()));
    }

    auto *da = new osg::DrawArrays(osg::PrimitiveSet::LINE_STRIP, 0, points->size());
    setVertexArray(points);
    addPrimitiveSet(da);
}

} // namespace nurbs

} // namespace osg
