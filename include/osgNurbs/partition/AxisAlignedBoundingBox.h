#pragma once

#include <utility>

#include "Core/EigenExtensions/EigenGeometryAndPlugins.h"

#include "Interval.h"

namespace LC
{

template <typename _Scalar, int _Dimension>
class AxisAlignedBoundingBox
{
public:
    using Scalar = _Scalar;
    using Point = Eigen::Matrix<Scalar, _Dimension, 1>;

    /**
     * @brief Dimensions
     * @todo Join with bezier::Curve::Dimension, for instance. Maybe put in Eigen::Matrix ?
     */
    enum Dimension: int {
        x = 0,
        y,
        z
    };

public:

    EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF_VECTORIZABLE_FIXED_SIZE(_Scalar, _Dimension)

    /// @brief Default constructor
    AxisAlignedBoundingBox(Point a, Point b)
            : _a { a }
            , _b { b }
    {}

    /// \name Copy ctors and operators
    ///@{
    AxisAlignedBoundingBox(AxisAlignedBoundingBox const& other) = default;
    AxisAlignedBoundingBox(AxisAlignedBoundingBox && other) noexcept = default;
    AxisAlignedBoundingBox &operator=(AxisAlignedBoundingBox const& other) = default;
    AxisAlignedBoundingBox &operator=(AxisAlignedBoundingBox && other) noexcept = default;
    ///@}

    ~AxisAlignedBoundingBox() = default;

    /// \name Accessors
    ///@{

    /**
     * @param dim Dimension. 0 for x, 1 for y, 2 for z
     * @todo Use an enum or something for dim
     * @return Return the interval of the AABB over dim.
     */
#if __cplusplus >= 201402L
    constexpr
#endif
    Interval<Scalar> spanOver(Dimension dim) const
    {
        return Interval<Scalar>(_a[dim], _b[dim]);
    }

    /**
     * @param dim The dimension
     * @return The points sorted over dim
     */
    std::pair<Point const&, Point const&> sortedPoints(Dimension dim) const
    {
            return _a[dim] < _b[dim] ? std::make_pair(_a, _b) : std::make_pair(_b, _a);
    }

    ///@}

private:
    Point _a, _b;
};

} // namespace LC.
