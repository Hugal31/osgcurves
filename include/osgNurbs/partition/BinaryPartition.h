#pragma once

#include <set>
#include <vector>

#include "AxisAlignedBoundingBox.h"
#include "osgNurbs/bezier/Curve.h"

namespace LC
{

// Forward declaration
class BinaryPartitionSerializer;

/**
 * @brief Represent a binary partition of bezier curves over a 2D parametric surface
 * @tparam _Scalar Real type
 */
template <typename _Scalar>
class BinaryPartition
{
    friend BinaryPartitionSerializer;

public:
    using Scalar = _Scalar;
    using BezierCurve = bezier::Curve<Scalar, 2>;
    using AABB = AxisAlignedBoundingBox<Scalar, 2>;


    // TODO Make this private and use a serializer
    struct Partition
    {
    public:
        Partition(Interval<Scalar> interval_, std::size_t index)
                : interval { interval_ }
                , curvesIndex { index } {}

        Partition(Interval<Scalar> interval_, std::set<std::size_t> indexes)
                : interval { interval_ }
                , curvesIndex { std::move(indexes) } {}

        Partition(Partition const&) = default;
        Partition(Partition &&) noexcept = default;
        Partition &operator=(Partition const&) = default;
        Partition &operator=(Partition &&) noexcept = default;

        constexpr bool operator<(Partition const& other) const
        {
            return interval < other.interval;
        }

    public:
        Interval<Scalar> interval;
        /// @brief Index of the curve to assert the intersection, in _curves. std::numeric_limits<Scalar>::max if no curve to consider.
        std::set<std::size_t> curvesIndex;
        /// @brief Number of certified intersections in this part
        // unsigned int nIntersection = 0;
    };

    struct PartitionOfPartition
    {
    public:
        PartitionOfPartition(Interval<Scalar> interval_)
                : interval { interval_ } {}

        PartitionOfPartition(PartitionOfPartition const&) = default;
        PartitionOfPartition(PartitionOfPartition &&) noexcept = default;
        PartitionOfPartition &operator=(PartitionOfPartition const&) = default;
        PartitionOfPartition &operator=(PartitionOfPartition &&) noexcept = default;

        constexpr bool operator<(PartitionOfPartition const& other) const
        {
            return interval < other.interval;
        }

    public:
        Interval<Scalar> interval;
        std::set<Partition> parts;
    };

    using Parts = std::set<PartitionOfPartition>;
    using PartIterator = typename Parts::const_iterator;

public:
    BinaryPartition(std::vector<BezierCurve> curves);
    ~BinaryPartition() = default;

    BinaryPartition(BinaryPartition const& other) = default;
    BinaryPartition(BinaryPartition && other) noexcept = default;

    BinaryPartition &operator=(BinaryPartition const& other) = default;
    BinaryPartition &operator=(BinaryPartition && other) noexcept = default;

    PartIterator begin() const { return _parts.begin(); }
    PartIterator end() const { return _parts.end(); }

private:

private:
    /// @brief Do the partition and fill _parts
    void partition();
    std::set<Partition> partitionOverY();

    /**
     * @brief Add an interval to a list of interval. If the interval collides with intervals in the list, divide them.
     * @param partitions List of partition
     * @param interval Interval to add
     * @param curveIndex Curve index corresponding to the interval.
     */
    static void addInterval(std::set<Partition> &partitions, Interval<Scalar> interval, std::size_t curveIndex);

    /// @brief Add empty partition between partitions, if they are not contiguous
    static void fillHoles(PartitionOfPartition &partition);

private:
    std::vector<BezierCurve> _curves;
    std::set<PartitionOfPartition> _parts;
};

} // namespace LC.

#include "BinaryPartition.inl"