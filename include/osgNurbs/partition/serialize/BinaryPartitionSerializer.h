#pragma once

#include <vector>

#include <osg/Vec4>
#include <osg/Vec4i>

#include "../BinaryPartition.h"

namespace LC
{

class BinaryPartitionSerializer
{
public:
    using Scalar = float;
    using Partition = BinaryPartition<Scalar>;

    struct SerializedPartition
    {
        std::vector<osg::Vec4i> trimmingCurvesDesc;
        std::vector<osg::Vec4> trimmingCurvesData;

        /// @brief [0] and [1] of each element contains the bounds of the interval
        std::vector<osg::Vec4> uIntervalsDesc;
        /**
         * @brief The index are pared with the uIntervalDesc.
         *        [0] and [1] of each element contains the bounds of the trimming curve list,
         *        and [2] contains the number of pre-classified intersections.
         */
        std::vector<osg::Vec4i> uIntervalsData;
        std::vector<osg::Vec4> vIntervalsDesc;
        std::vector<osg::Vec4i> trimmingCurveList;
    };

public:
    static SerializedPartition serialize(Partition const& binaryPartition);

private:
    static float intAsFloatBits(int i)
    {
        return *reinterpret_cast<float*>(&i);
    }

    static float intAsFloatBits(unsigned int i)
    {
        return *reinterpret_cast<float*>(&i);
    }
};

}
