#pragma once

#include <cassert>

#include "BinaryPartition.h"

namespace LC
{

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar>
BinaryPartition<_Scalar>::BinaryPartition(std::vector<BezierCurve> curves)
        : _curves { std::move(curves) }
{
    partition();
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar>
void BinaryPartition<_Scalar>::partition()
{
    std::set<Partition> yParts = partitionOverY();

    // For each y interval
    //   Do aproximately the same thing than in partitionOverY
    for (Partition const& yPart: yParts) {

        PartitionOfPartition yPartionOfPartition { yPart.interval };

        for (std::size_t curveIndex: yPart.curvesIndex) {
            BezierCurve const& curve = _curves[curveIndex];

            // Compute the bounds of the curve in the y-interval, as U parameter
            Scalar low, high;
            assert(curve.bisect(BezierCurve::Dimension::y, yPart.interval.low(), low));
            assert(curve.bisect(BezierCurve::Dimension::y, yPart.interval.high(), high));

            // Compute the bounds of the curve in the x-interval.
            Interval<Scalar> curveXInterval { curve(low).x(), curve(high).x() };

            addInterval(yPartionOfPartition.parts, curveXInterval, curveIndex);
        }

        fillHoles(yPartionOfPartition);

        _parts.insert(std::move(yPartionOfPartition));
    }
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar>
std::set<typename BinaryPartition<_Scalar>::Partition> BinaryPartition<_Scalar>::partitionOverY()
{
    std::set<Partition> y_intervals;

    for (std::size_t currentCurveIndex = 0; currentCurveIndex < _curves.size(); ++currentCurveIndex) {
        BezierCurve const& currentCurve = _curves[currentCurveIndex];
        // Intervals where only this currentCurve is spanning
        Interval<Scalar> curveInterval { currentCurve(0).y(), currentCurve(1).y() };

        addInterval(y_intervals, curveInterval, currentCurveIndex);
    }

    return y_intervals;
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar>
void BinaryPartition<_Scalar>::addInterval(std::set<Partition> &partitions, Interval<Scalar> interval, std::size_t curveIndex)
{
    std::set<Interval<Scalar>> curveIntervals { { interval } };

    // Check potentially intersecting intervals
    // For each interval in y_interval
    //   For each interval in curveIntervals
    //     If they are overlapping, there is A, the curve of y_intervals, and C, currentCurve
    //     Remove the A interval from y_intervals
    //     Cut the intervals in at most 3 parts
    //     Put the interval with only the curve A back in y_intervals
    //     Put the interval with the two curves in y_intervals, with the two curves indexes
    //     Put the remaining interval(s) into curveIntervals
    auto partition = partitions.begin();
    while (partition != partitions.end() && !curveIntervals.empty()) {
        std::set<Interval<Scalar> > newCurveIntervals;

        for (Interval<Scalar> const& curveInterval: curveIntervals) {
            if (curveInterval.overlapsWith(partition->interval)) {
                Partition otherInterval = *partition;

                // Remove it from partitions
                partitions.erase(partition++);

                Interval<Scalar> middleInterval = curveInterval.intersection(otherInterval.interval);
                {
                    // Add the middle intersection, with the two curve indexes
                    std::set<std::size_t> curvesIndex = otherInterval.curvesIndex;
                    curvesIndex.insert(curveIndex);
                    partitions.insert(Partition(middleInterval, curvesIndex));
                }

                // Add the upper interval
                if (curveInterval.high() > otherInterval.interval.high()){
                    // The currentCurve has the upper intervals
                    Interval<Scalar> currentCurveTopInterval { otherInterval.interval.high(), curveInterval.high() };
                    newCurveIntervals.insert(currentCurveTopInterval);
                } else if (curveInterval.high() < otherInterval.interval.high()) {
                    // The other curve has the upper interval
                    Partition otherTopInterval = otherInterval;
                    otherTopInterval.interval.clipLow(curveInterval.high());
                    partitions.insert(otherTopInterval);
                }

                // Add the lower interval
                if (curveInterval.low() < otherInterval.interval.low()){
                    // The currentCurve has the lower intervals
                    Interval<Scalar> currentCurveLowInterval { curveInterval.low(), otherInterval.interval.low() } ;
                    newCurveIntervals.insert(currentCurveLowInterval);
                } else if (curveInterval.low() > otherInterval.interval.low()) {
                    // The other curve has the upper interval
                    Partition otherLowInterval = otherInterval;
                    otherLowInterval.interval.clipHigh(curveInterval.low());
                    partitions.insert(otherLowInterval);
                }

                break;
            } else {
                newCurveIntervals.insert(curveInterval);
                ++partition;
            }
        }

        curveIntervals = newCurveIntervals;
    }

    for (Interval<Scalar> const& i: curveIntervals)
        partitions.insert(Partition { i, curveIndex });
}

////////////////////////////////////////////////////////////////////////////////
template <typename _Scalar>
void BinaryPartition<_Scalar>::fillHoles(PartitionOfPartition &partition)
{
    Scalar lastIntervalHigh;

    auto partIt = partition.parts.begin();
    lastIntervalHigh = partIt->interval.high();
    ++partIt;

    for (; partIt != partition.parts.end(); ++partIt) {
        Scalar intervalLow = partIt->interval.low();
        if (lastIntervalHigh != intervalLow) {
            partition.parts.emplace_hint(partIt, Interval<Scalar> { lastIntervalHigh, intervalLow }, std::set<std::size_t>{});
        }
        lastIntervalHigh = partIt->interval.high();
    }
}

} // namespace LC.
