#pragma once

#include <utility>

namespace LC
{

/**
 * @brief Data type to represent invervals. At any moment, low() <= high()
 * @tparam _Scalar A Real type
 */
template<typename _Scalar>
class Interval
{
public:
    using Scalar = _Scalar;

public:

    /**
     * @brief Main constructor. Swap low and high if they are not sorted.
     */
#if __cplusplus >= 201402L
    // Add constexpr if C++ 14 is available
    constexpr
#endif
    Interval(Scalar low, Scalar high)
            : _low { low }
            , _high { high }
    {
        if (_low > _high)
            std::swap(_low, _high);
    }
    constexpr Interval(const Interval &other) noexcept = default;

    Interval &operator=(const Interval &other) noexcept = default;

    /// \name Accessors and other const functions
    ///@{
    Scalar low() const { return _low; }
    Scalar high() const { return _high; }

    constexpr bool contains(Scalar value) const { return value >= _low && value <= _high; }

    /// \return Return true if other is totally in *this
    constexpr bool contains(Interval const& other) const { return _low <= other._low && _high >= other._high; }

    /// \return True if the two intervals overlaps
    constexpr bool overlapsWith(Interval const& other) const { return _low < other._high && _high > other._low; }

    /// \brief Less operator to sort in std::set. Sort first by low(), then by high().
    constexpr bool operator<(Interval const& other) const
    {
        return _low < other._low || (_low == other._low && _high < other._high);
    }

    constexpr bool operator>(Interval const& other) const
    {
        return _low > other._low || (_low == other._low && _high > other._high);
    }

    constexpr bool operator!=(Interval const& other) const
    {
        return _low != other._low || _high == other._high;
    }

    constexpr bool operator==(Interval const& other) const
    {
        return !(*this != other);
    }

    constexpr Scalar distance() const { return _high - _low; }

    /**
     * @throws std::domain_error if the two intervals do not overlap.
     * @return The intersection of the two intervals.
     */
    Interval intersection(Interval const& other) const
    {
        if (!overlapsWith(other))
            throw std::domain_error("Cannot do an interval union if they do not overlaps");

        return Interval(std::max(_low, other._low), std::min(_high, other._high));
    }

    ///@}

    /// \name Settors
    ///@{

    /// \brief Clip the upper bound of the interval to value. As no effect if low() > value
    constexpr void clipHigh(Scalar value)
    {
        if (_low <= value)
            _high = value;
    }

    /// \brief Clip the lower bound of the interval to value. As no effect if high() < value
    constexpr void clipLow(Scalar value)
    {
        if (_high >= value)
            _low = value;
    }

    ///@}

private:
    _Scalar _low, _high;
};

} // namespace LC.